import matplotlib.pyplot as plt
import numpy as np
import random

from numpy.random import beta as np_beta
from scipy.stats import beta as sci_beta
from scipy.stats import bernoulli


class MonteCarloSimulation:
    credit_ratings = ['AAA', 'AA', 'A', 'BBB', 'BB', 'B', 'CCC']  # all possible credit ratings
    credit_rating_pd = {  # probability default for each credit rating
        'AAA': 0.00002,
        'AA': 0.0001,
        'A': 0.0005,
        'BBB': 0.0024,
        'BB': 0.012,
        'B': 0.0604,
        'CCC': 0.3041,
    }
    reinsurance_companies = []
    shock = 0.0

    def __init__(self, k, alpha, tao):
        # initial parameters
        self.k = k  # number of reinsurance companies
        self.alpha = alpha
        self.tao = tao

        # prepare everything for a simulation
        self.generate_reinsurance_companies()
        self.generate_shock()

    def generate_reinsurance_companies(self):
        # for each reinsurance company, get a credit rating
        for i in range(self.k):
            credit_rating = random.choice(self.credit_ratings)
            credit_rating_pd = self.credit_rating_pd[credit_rating]
            print i, credit_rating, self.calculate_w(credit_rating_pd), self.calculate_y()
            self.reinsurance_companies.append(credit_rating)

    def generate_shock(self):
        self.shock = np_beta(self.alpha, 1)  # beta = 1

    def calculate_b(self, cr):
        p = self.credit_rating_pd[cr]
        return (self.tao * p) / (self.alpha * (1 - p) + self.tao)

    def calculate_w(self, p):
        """
        If True, then the RE company defaults.
        """
        return bernoulli.rvs(p)

    def calculate_y(self):
        return 1.0/self.k

    def cov(self, b1, b2, p1, p2):
        return (self.alpha * (1-b1) * (1-b2))/(self.alpha + (self.tao/b1) + (self.tao/b2)) - (p1-b1)*(p2-b2)

    def calculate_p(self, cr):
        """
        Calculate probability default for specific counterparty.
        cr = credit_rating
        """
        b = self.calculate_b(cr)
        return b + (1 - b) * self.shock**(self.tao/b)

    def print_values(self):
        print "-----------------------------"
        print "Reinsurance companies: "
        for re in self.reinsurance_companies:
            print re,
        print
        print "Shock: %s" % self.shock
        print "-----------------------------"

# k = 3......100

#draw_beta_distribtion()

mcs = MonteCarloSimulation(k=10, alpha=0.1, tao=0.2)
#mcs.print_values()


#for i in range(100):
#    mcs = MonteCarloSimulation(k=1, alpha=0.1, tao=0.2)
#    print mcs.shock


def draw_beta_distribtion():
    fig, ax = plt.subplots(1, 1)
    a, b = 0.1, 1
    mean, var, skew, kurt = sci_beta.stats(a, b, moments='mvsk')

    x = np.linspace(sci_beta.ppf(0.01, a, b), sci_beta.ppf(0.99, a, b), 100)

    ax.plot(x, sci_beta.pdf(x, a, b), 'r-', lw=5, alpha=0.6, label='beta pdf')

    plt.show()
