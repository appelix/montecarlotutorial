def progress_bar_printer(current, total):
    """
    Prints progress bar for 'current' step out of 'total'.
    """
    progress = int(100 * (current * 1.0 / (total * 1.0)))
    print '\r[{0}{1}] {2}%'.format('#' * (progress / 5), ' ' * (20 - progress / 5), progress),
    if current == total:
        print


"""
def get_w(self):
    w = []
    for re in self.re_companies:
        w.append(re.w)
    return np.array(w)

def get_y(self):
    y = []
    for re in self.re_companies:
        y.append(re.y)
    return np.array(y)

def calculate_variance(self):
    y = self.get_y()
    cov = self.create_cov_matrix()
    return np.dot(np.dot(y, cov), y)

def calculate_z(self):
    return np.dot(self.get_w(), self.get_y())

def create_cov_matrix(self):
    # prepare empty 2D array
    cov = [[0.0 for i in range(len(self.re_companies))] for j in range(len(self.re_companies))]

    for i, re1 in enumerate(self.re_companies):
        for j, re2 in enumerate(self.re_companies):
            if i == j:
                cov[i][j] = re1.p * (1 - re1.p)
            else:
                cov[i][j] = (self.alpha * (1-re1.b) * (1-re2.b))/(self.alpha + (self.tao/re1.b) + (self.tao/re2.b)) - (re1.p-re1.b)*(re2.p-re2.b)

    return np.array(cov)

def print_values(self):
    print "CR \t p \t\t b \t\t w \t y"
    print "---------------------------------------------------------"
    for re in self.re_companies:
        print "%s \t %.5f \t %.5f \t %d \t %.4f" % (re.cr, re.p, re.b, re.w, re.y)
    print "---------------------------------------------------------"
    print "Z: %f" % self.calculate_z()
    print "Variance: %f" % self.calculate_variance()
    print "---------------------------------------------------------"
"""