import csv
import matplotlib.pyplot as plt
import numpy as np
import random

from numpy.random import beta as np_beta
from scipy.stats import beta as sci_beta
from scipy.stats import bernoulli

from utils import progress_bar_printer

# all possible credit ratings
CREDIT_RATINGS = ['AAA', 'AA', 'A', 'BBB', 'BB', 'B', 'CCC']

# default probability for each credit rating, source: QIS5
QIS5 = {
    'AAA': 0.00002,
    'AA': 0.0001,
    'A': 0.0005,
    'BBB': 0.0024,
    'BB': 0.012,
    'B': 0.0604,
    'CCC': 0.3041,
}


class REInsurance:
    def __init__(self, credit_rating, alpha, tao, k):
        self.cr = credit_rating
        self.p = QIS5[self.cr]
        self.b = (tao * self.p) / (alpha * (1 - self.p) + tao)
        self.w = bernoulli.rvs(self.p)
        self.y = 1.0/k


class MonteCarloSimulation:
    def __init__(self, k, alpha, tao):
        # initial parameters
        self.k = k  # number of reinsurance companies
        self.alpha = alpha
        self.tao = tao
        self.re_companies = []
        self.shock = 0.0

        # prepare everything for a simulation
        self.generate_shock()
        self.generate_reinsurance_companies()

    def generate_reinsurance_companies(self):
        for i in range(self.k):
            credit_rating = random.choice(CREDIT_RATINGS)
            self.re_companies.append(REInsurance(credit_rating, self.alpha, self.tao, self.k))

    def generate_shock(self):
        self.shock = np_beta(self.alpha, 1)  # beta = 1

    def get_w(self):
        w = []
        for re in self.re_companies:
            w.append(re.w)
        return np.array(w)

    def get_y(self):
        y = []
        for re in self.re_companies:
            y.append(re.y)
        return np.array(y)

    def calculate_variance(self):
        y = self.get_y()
        cov = self.create_cov_matrix()
        return np.dot(np.dot(y, cov), y)

    def calculate_z(self):
        return np.dot(self.get_w(), self.get_y())

    def create_cov_matrix(self):
        # prepare empty 2D array
        cov = [[0.0 for i in range(len(self.re_companies))] for j in range(len(self.re_companies))]

        for i, re1 in enumerate(self.re_companies):
            for j, re2 in enumerate(self.re_companies):
                if i == j:
                    cov[i][j] = re1.p * (1 - re1.p)
                else:
                    cov[i][j] = (self.alpha * (1-re1.b) * (1-re2.b))/(self.alpha + (self.tao/re1.b) + (self.tao/re2.b)) - (re1.p-re1.b)*(re2.p-re2.b)

        return np.array(cov)

    def print_values(self):
        print "CR \t p \t\t b \t\t w \t y"
        print "---------------------------------------------------------"
        for re in self.re_companies:
            print "%s \t %.5f \t %.5f \t %d \t %.4f" % (re.cr, re.p, re.b, re.w, re.y)
        print "---------------------------------------------------------"
        print "Z: %f" % self.calculate_z()
        print "Variance: %f" % self.calculate_variance()
        print "---------------------------------------------------------"


class MonteCarloSimulationExample(MonteCarloSimulation):
    """
    Special7 case: example from article: Portfolio modelling of counterparty reinsurance default risk.
    """

    def generate_reinsurance_companies(self):
        ratings = ['AAA', 'AAA', 'AA', 'AA', 'A', 'A', 'BBB', 'BBB', 'BB', 'BB', 'B', 'B', 'CCC', 'CCC']

        for i in range(self.k):
            credit_rating = ratings[i]
            self.re_companies.append(REInsurance(credit_rating, self.alpha, self.tao, self.k))


class MonteCarloSimulation2(MonteCarloSimulation):

    def generate_reinsurance_companies(self):
        """
        Generate 7*200 RE insurance companies. K should be 1400.
        """

        for i in range(7):
            for j in range(200):
                credit_rating = CREDIT_RATINGS[i]
                self.re_companies.append(REInsurance(credit_rating, self.alpha, self.tao, self.k))


class Simulator:
    def __init__(self, iterations, alpha, tao, monte_carlo_class):
        self.iterations = iterations
        self.alpha = alpha
        self.tao = tao
        self.results = []
        self.MonteCarloClass = monte_carlo_class

    def run_simulation(self):
        for i in range(self.iterations):
            mcs = self.MonteCarloClass(k=144, alpha=self.alpha, tao=self.tao)
            self.results.append(mcs.calculate_z())

    def draw_histogram(self):
        plt.hist(self.results, bins=100, color='c')
        plt.axvline(np.array(self.results).mean(), color='b', linestyle='dashed', linewidth=2)
        print "Graph has been drown."
        plt.show()


s = Simulator(iterations=1000, alpha=0.1, tao=0.2, monte_carlo_class=MonteCarloSimulation)
s.run_simulation()
s.draw_histogram()

#s = RandomSimulator(iterations=10000, alpha=0.1, tao=0.2)
#s.run_simulation()
#s.draw_histogram()


#mcs = MonteCarloSimulation(k=1000, alpha=0.1, tao=0.2)
#mcs.print_values()

#mcs = MonteCarloSimulationExample(k=14, alpha=0.1, tao=0.2)
#mcs.print_values()
