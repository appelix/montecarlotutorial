import random
import matplotlib
import matplotlib.pyplot as plt


# let us go ahead and change this to return a simple win/loss
def rollDice():
    roll = random.randint(1, 100)

    if roll == 100:
        #print roll, 'roll was 100, you lose. What are the odds?! Play again!'
        return False
    elif roll <= 50:
        #print roll, 'roll was 1-50, you lose.'
        return False
    elif 100 > roll >= 50:
        #print roll, 'roll was 51-99, you win! *pretty lights flash* (play more!)'
        return True


def simple_bettor(funds, initial_wager, wager_count):
    value = funds
    wager = initial_wager

    # wager x
    wX = []

    # value y
    vY = []

    currentWager = 1

    while currentWager <= wager_count:
        if rollDice():
            value += wager
            wX.append(currentWager)
            vY.append(value)
        else:
            value -= wager
            wX.append(currentWager)
            vY.append(value)

        currentWager += 1

    plt.plot(wX, vY)

    # changed to reduce spam
    #if value < 0:
    #    value = 'Broke!'

    #print "Funds:", value

x = 0

while x < 10:
    simple_bettor(10000, 100, 10000)
    x += 1


plt.ylabel("Account value")
plt.xlabel("Wager count")
plt.show()
