# -*- coding: utf-8 -*-

from collections import Counter, defaultdict
from math import sqrt
from random import choice, randint

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import percentile
from numpy.random import beta as np_beta
from numpy.random import lognormal
from scipy.interpolate import UnivariateSpline
from scipy.stats import bernoulli
from scipy.stats import stats

from data import RE_COMPANIES_DATA


# Constants
ALPHA = 2.5
TAO = 1
GAMMA = ALPHA/TAO
B = "p/(GAMMA*(1-p)+1)"

# Default probability for each credit rating
DEFAULT_PROBABILITIES = {
    'AAA': 0.00002,
    'AA': 0.0001,
    'A': 0.0005,
    'BBB': 0.0024,
    'BB': 0.012,
    'B': 0.042,
    'CCC': 0.042,
}

# Override matplotlib default settings
matplotlib.rc('font', family='Times New Roman')


class REInsurance:
    """
    RE Insurance Company:
    - cr = credit rating ['AAA', 'AA', 'A', 'BBB', 'BB', 'B', 'CCC']
    - lgd = Loss given default
    """

    def __init__(self, lgd, p, credit_rating):
        self.lgd = lgd
        self.p = p
        self.credit_rating = credit_rating
        self.b = (TAO * p) / (ALPHA * (1 - p) + TAO)

    def ps(self, shock):
        """
        Default probabibility dependent on shock size.
        """
        return self.b + (1-self.b)*shock**(TAO/self.b)

    def default(self, shock):
        """
        Return 1 if the RE company defaults, 0 otherwise.

        Example for uniform:
        return 1 if uniform(0, 1) <= self.ps(shock) else 0
        """
        return bernoulli.rvs(self.ps(shock))


class MonteCarloIteration:
    def __init__(self, re_companies):
        self.re_companies = re_companies
        self.shock = np_beta(ALPHA, 1)  # beta = 1

    def run_iteration(self):
        return sum([re.lgd for re in self.re_companies if re.default(self.shock) == 1])  # noqa

    def print_values(self):
        print "---------------------------------------------------------"
        print "Shock: %f" % self.shock
        print "---------------------------------------------------------"
        print "CR \t b \t\t ps \t default"
        print "---------------------------------------------------------"
        for re in self.re_companies:
            print "%s \t %.5f \t %.4f \t %.4f" % (re.cr, re.b, re.ps(self.shock), re.default(self.shock))  # noqa
        print "---------------------------------------------------------"


class Simulator:

    def __init__(self):
        self.re_companies = []
        self.results = []

    def generate_re_companies(self, no_of_companies, custom_distribution):
        raise NotImplemented

    def run_simulation(self, iterations):
        for i in range(iterations):
            mci = MonteCarloIteration(re_companies=self.re_companies)
            sum_lgd = mci.run_iteration()
            self.results.append(sum_lgd)

    def draw_histogram(self, description=""):
        p, x = np.histogram(self.results, bins=400)
        plt.hist(self.results, bins=400, color='c', alpha=0.5)
        plt.axvline(self.calculate_empirical_scr(),
                    color='red', linestyle='dashed', linewidth=2,
                    label=u'99.5% kvantil')
        plt.xlabel("Izpostavljenost")
        plt.ylabel("Frekvenca")

        if description:
            plt.suptitle(description, fontsize=14, fontweight="bold")

        # density line
        x = x[:-1] + (x[1] - x[0]) / 2
        f = UnivariateSpline(x, p, s=int(len(self.results)/3))
        plt.plot(x, f(x), linewidth=2)

        print "Graph has been drawn."
        plt.show()

    def create_cov_matrix(self):
        # prepare empty 2D array
        dim = len(self.re_companies)
        cov = [[0.0 for _ in range(dim)] for _ in range(dim)]

        for i, re1 in enumerate(self.re_companies):
            for j, re2 in enumerate(self.re_companies):
                if i == j:
                    cov[i][j] = re1.p * (1 - re1.p)
                else:
                    cov[i][j] = (ALPHA * (1-re1.b) * (1-re2.b))/(ALPHA + (TAO/re1.b) + (TAO/re2.b)) - (re1.p-re1.b)*(re2.p-re2.b)  # noqa

        return np.array(cov)

    def get_lgds(self):
        """ Prepare lgds vector """
        return np.array([re.lgd for re in self.re_companies])

    def get_p(self):
        """ Prepare p vector """
        return np.array([re.p for re in self.re_companies])

    def calculate_variance(self):
        lgds = self.get_lgds()
        cov = self.create_cov_matrix()
        return np.dot(np.dot(lgds, cov), lgds)

    def calculate_mean(self):
        return np.array(self.results).mean()

    def calculate_expected_loss(self):
        return np.dot(self.get_lgds(), self.get_p())

    def calculate_scr(self):
        capital_requirement = 0.0
        sum_lgds = sum(self.get_lgds())

        sd = sqrt(self.calculate_variance())  # standard deviation

        if sd <= 0.07 * sum_lgds:
            capital_requirement = 3 * sd
        elif 0.07 * sum_lgds < sd <= 0.2 * sum_lgds:
            capital_requirement = 5 * sd
        elif 0.2 * sum_lgds <= sd:
            capital_requirement = sum_lgds

        return capital_requirement

    def calculate_empirical_scr(self):
        return percentile(np.array(self.results), 99.5)

    def credit_rating_stats(self):
        credit_ratings = [re.credit_rating for re in self.re_companies]
        return Counter(credit_ratings)

    def calculate_lgd_exposure_per_credit_rating(self):
        lgds_by_cr = defaultdict(list)
        stats = defaultdict(float)

        for re_company in self.re_companies:
            lgds_by_cr[re_company.credit_rating].append(re_company.lgd)

        for cr, lgds in lgds_by_cr.iteritems():
            stats[cr] = sum(lgds)

        return stats

    def calculate_standard_deviation(self):
        return np.std(np.array(self.results))

    def calculate_skewness(self):
        return stats.skew(np.array(self.results))

    def calculate_kurtosis(self):
        return stats.kurtosis(np.array(self.results))


class SimulatorRealData(Simulator):
    def generate_re_companies(self, no_of_companies, _):
        for p, lgd, credit_rating in RE_COMPANIES_DATA:
            re = REInsurance(lgd, p, credit_rating)
            self.re_companies.append(re)


class SimulatorRandomCompanies(Simulator):
    def generate_re_companies(self, no_of_companies, _):
        for i in range(no_of_companies):
            r = randint(0, 143)
            p, lgd, credit_rating = RE_COMPANIES_DATA[r]
            re = REInsurance(lgd, p, credit_rating)
            self.re_companies.append(re)


class SimulatorRandomLGDs(Simulator):
    def generate_re_companies(self, no_of_companies, _):
        for i in range(no_of_companies):
            lgd = lognormal()
            credit_rating = choice(DEFAULT_PROBABILITIES.keys())
            p = DEFAULT_PROBABILITIES[credit_rating]
            re = REInsurance(lgd, p, credit_rating)
            self.re_companies.append(re)


class SimulatorRandomLGDsCustomRatingDistribution(Simulator):
    def generate_re_companies(self, no_of_companies, custom_distribution):
        self.re_companies = self._generate_custom_dist(custom_distribution)
        assert len(self.re_companies) == no_of_companies

    @staticmethod
    def _generate_custom_dist(distribution):
        re_companies = []
        for credit_rating in distribution.keys():
            count = distribution[credit_rating]
            for _ in range(count):
                lgd = lognormal()
                p = DEFAULT_PROBABILITIES[credit_rating]
                re_companies.append(REInsurance(lgd, p, credit_rating))
        return re_companies


class SingleExample(object):

    def __init__(self, simulator, description, no_of_companies=144, iterations=10000):  # noqa
        self.simulator = simulator
        self.description = description
        self.no_of_companies = no_of_companies
        self.iterations = iterations

    def start(self, custom_distribution=False):
        print "----- %s -----" % self.description
        s = self.simulator()
        s.generate_re_companies(self.no_of_companies, custom_distribution)
        print "Alpha: %f" % ALPHA
        print "LGD exposure statistics: %s" % s.calculate_lgd_exposure_per_credit_rating()  # noqa
        print "Credit rating stats: %s" % str(s.credit_rating_stats())
        s.run_simulation(self.iterations)
        print "Standard deviation (mathematical): %f" % s.calculate_standard_deviation()  # noqa
        print "Iterations: %d" % self.iterations
        print "Count insurances: %d" % len(s.re_companies)
        print "Expected loss: %f" % s.calculate_expected_loss()
        print "Capital requirement (SCR): %f" % s.calculate_scr()
        print "Capital requirement (Empirical SCR - 99.5): %f" % s.calculate_empirical_scr()  # noqa
        print "Variance: %f (sqrt: %f)" % (s.calculate_variance(), sqrt(s.calculate_variance()))  # noqa
        print "Mean: %f" % s.calculate_mean()
        print "Skewness: %f" % s.calculate_skewness()
        print "Kurtosis: %f" % s.calculate_kurtosis()
        s.draw_histogram(self.description)


class MultipleExamples(SingleExample):

    def __init__(self, simulator, description, no_of_companies=144,
                 iterations=10000, examples=10):
        super(MultipleExamples, self).__init__(simulator, description,
                                               no_of_companies, iterations)
        self.examples = examples
        self.variances = []
        self.expected_losses = []

    def start(self):
        for example in range(self.examples):
            s = self.simulator()
            s.generate_re_companies(self.no_of_companies)

            variance = s.calculate_variance()
            expected_loss = s.calculate_expected_loss()
            self.variances.append(sqrt(variance))
            self.expected_losses.append(expected_loss)

        print "Variances: %s" % str(self.variances)
        print "Expected losses: %s" % str(self.expected_losses)

    def draw_chart(self):
        # calculate min/max variances
        min_variance = min(self.variances)
        min_variance_index = self.variances.index(min_variance)
        print "Min variance (value=%f, index=%d)" % (
            min_variance,
            min_variance_index,
        )
        max_variance = max(self.variances)
        max_variance_index = self.variances.index(max_variance)
        print "Max variance (value=%f, index=%d)" % (
            max_variance,
            max_variance_index,
        )

        plt.plot(self.variances)
        # min line
        plt.plot(range(len(self.variances)),
                 [min_variance for _ in range(len(self.variances))], 'r--')
        # max line
        plt.plot(range(len(self.variances)),
                 [max_variance for _ in range(len(self.variances))], 'b--')
        plt.ylabel('Variances')
        plt.show()

        print "Variance chart has been drawn."

        # calculate min/max expected losses
        min_loss = min(self.expected_losses)
        min_loss_index = self.expected_losses.index(min_loss)
        print "Min expected loss (value=%f, index=%d)" % (
            min_loss,
            min_loss_index,
        )
        max_loss = max(self.expected_losses)
        max_loss_index = self.expected_losses.index(max_loss)
        print "Max loss index (value=%f, index=%d)" % (
            max_loss,
            max_loss_index,
        )

        plt.plot(self.expected_losses)
        # min line
        plt.plot(range(len(self.expected_losses)),
                 [min_loss for _ in range(len(self.expected_losses))], 'r--')
        # max line
        plt.plot(range(len(self.expected_losses)),
                 [max_loss for _ in range(len(self.expected_losses))], 'b--')
        plt.ylabel('Expected losses')
        plt.show()

        print "Expected losses chart has been drawn."


multiple_example1 = MultipleExamples(
    simulator=SimulatorRandomLGDs,
    description="1000 RE Companies randomly distributed into rating classes",
    no_of_companies=1000,
    examples=10,
)

example1 = SingleExample(
    simulator=SimulatorRealData,
    description="All 144 RE Companies (real data)",
    no_of_companies=144,
    iterations=100000,
)

example2 = SingleExample(
    simulator=SimulatorRandomCompanies,
    description="1000 random RE insurances",
    no_of_companies=1000,
    iterations=100000,
)

example3 = SingleExample(
    simulator=SimulatorRandomLGDs,
    description="1000 RE Companies randomly distributed into rating classes",
    no_of_companies=1000,
    iterations=1000,
)

example4 = SingleExample(
    simulator=SimulatorRandomLGDsCustomRatingDistribution,
    description="1000 RE companies with known rating distribution",
    no_of_companies=1000,
    iterations=100000,
)

multiple_example1.start()
multiple_example1.draw_chart()

example1.start()
example2.start()
example3.start()

# Custom distributions
example4.start(custom_distribution={'AAA': 1000})
example4.start(custom_distribution={'AAA': 999, 'B': 1})
example4.start(custom_distribution={'B': 1000})
example4.start(custom_distribution={'AAA': 1, 'B': 999})
example4.start(custom_distribution={'A': 500, 'BBB': 500})
